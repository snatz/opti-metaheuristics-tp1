% Add subpaths to Matlab global path. Allows global usage of GA functions.
% Only need to be executed once.
addpath(genpath('.'));

profile off;
profile clear;
profile on;

%problem = rosenbrockProblem();
problem = griewankProblem();

selectionFn = @rouletteWheelSelection;
crossoverFn = @singlePointCrossover;
mutationFn = @bitflipMutation;
narrowFn = @strictNarrow;
stopFn = @fitnessValueStop;

% Population length, max generation, chrom. length, crossover prob.
% mutation prob., lambda, additional arguments for selection, crossover and
% mutation.
config = geneticConfig(100, 400, 16, 0.9, 0.05, 2, ...
    [], [problem.constraints(1, :)], [problem.constraints(1, :)], [-0.08]);

maximizing = 0;

[result, history] = optimize(problem.objectiveFn, selectionFn, crossoverFn, ...
    mutationFn, narrowFn, stopFn, problem.constraints, config, maximizing);

profile viewer;

disp(result);
