function f = bin2float(b, minValue)
    f = bin2dec(b) / 10^3;
    
    if (minValue < 0)
        f = f - abs(minValue);
    end
end