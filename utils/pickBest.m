function result = pickBest(popX, popY, fitnessFn)
    best = -Inf;
    idx = 0;
    
    for i = 1:length(popX)
        f = fitnessFn(popX(i), popY(i));
        if (best < f)
            best = f;
            idx = i;
        end
    end
    
    result(1) = popX(idx);
    result(2) = popY(idx);
    result(3) = best;
end

