function b = float2bin(f, L, minValue)
    if (minValue < 0)
        f = f + abs(minValue);
    end
    
    b = num2str(dec2bin(round(f, 4) * 10^3, L));
end