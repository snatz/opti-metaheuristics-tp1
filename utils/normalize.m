function norm = normalize(fitnesses)
    len = length(fitnesses);
    norm = zeros(len, 1);
    minFit = min(fitnesses);
    
    if (minFit < 0)
        for i = 1:len
            norm(i) = fitnesses(i) + 2*abs(minFit);
        end
    else
        norm = fitnesses;
    end
end