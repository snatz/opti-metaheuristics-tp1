function pop = initPopulation(N, min, max)
    % Initialize the first generation.
    pop = min + rand(N, 1) * (max - min);
end