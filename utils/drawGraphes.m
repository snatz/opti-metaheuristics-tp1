function drawGraphes(fitnessFn, constraints, history, result, maximizing)
    [Xfit, Yfit] = meshgrid(constraints(1, 1):.1:constraints(1, 2), ...
        constraints(2, 1):.1:constraints(2, 2));
    Zfit = fitnessFn(Xfit, Yfit);

    if (maximizing == 1)
        [~, idx] = max(history(:, 3));
    else
        [~, idx] = min(history(:, 3));
    end
    
    % (Iterations, Fitness) + Best individual
    figure(1);
    clf;
    plot(linspace(1, length(history), length(history)), history(:, 3));
    hold on;
    plot(idx, result(3), 'Color', 'red', 'Marker', '+');
    hold off;
    
    % (X, Y, Fitness) + Best individual
    figure(2);
    clf;
    mesh(Xfit, Yfit, Zfit);
    hold on;
    plot3(result(1), result(2), result(3), 'Color', 'red', 'Marker', '+');
    hold off;
end
