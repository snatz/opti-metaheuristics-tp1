function pop = normalNMutation(pop, mutateProb, l, args)
% NORMALMUTATION  Apply the normal (N) mutation operation to the population.
%   The argument needed is sigma (the standard deviation of a normal 
%   distribution (one per individual).

    sigma = args(1);
    N = length(pop);
    
    if (~isvector(sigma) || length(sigma) ~= N)
        sigma = rand(N, 1);
    end
    
    for i = 1:length(pop)
        if (rand < mutateProb)
            pop(i) = pop(i) + normrnd(0, 1) * sigma(i);
        end
    end
end