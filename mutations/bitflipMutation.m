function pop = bitflipMutation(pop, mutateProb, l, args)
% BITFLIPMUTATION  Apply the bitflip mutation operation to the population.
%   The arguments needed are the boundaries.

    constraints = args(1);

    for i = 1:length(pop)
        if (rand < mutateProb)
            b = float2bin(pop(i), l, constraints(1));
            idx = randi(l);
            
            if (b(idx) == '0')
                b(idx) = '1';
            else
                b(idx) = '0';
            end
            
            pop(i) = bin2float(b, constraints(1));
        end
    end
end