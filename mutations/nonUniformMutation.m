function pop = nonUniformMutation(pop, mutateProb, l, args)
% NONUNIFORMMUTATION  Apply the non-uniform mutation operation to the
%   population. The arguments needed are the number of max generations, the
%   recuit control parameter, and the boundaries.

    persistent g;
        
    Gmax = args(1);
    b = args(2);
    lb = args(3);
    ub = args(4);
    
    if (g < Gmax)
        g = g + 1;
    else
        g = 1;
    end
    
    for i = 1:length(pop)
        if (rand < mutateProb)
            if (rand >= 0.5)
                delta = mutationExt(g, Gmax, ub - pop(i), b);
            else
                delta = -1*mutationExt(g, Gmax, pop(i) - lb, b);
            end
            
            pop(i) = pop(i) + delta;
        end
    end
end

function d = mutationExt(g, Gmax, y, b)
    inv = ((1-g) / Gmax)^b;
    d = y * (1 - rand^inv);
end