function pop = boundaryMutation(pop, mutateProb, l, args)
% BOUNDARYMUTATION  Apply the boundary mutation operation to the population.
%   The arguments needed are the boundaries.

    lowerBound = args(1);
    upperBound = args(2);

    for i = 1:length(pop)
        if (rand < mutateProb)
            if (rand <= 0.5)
                pop(i) = lowerBound;
            else
                pop(i) = upperBound;
            end
        end
    end
end