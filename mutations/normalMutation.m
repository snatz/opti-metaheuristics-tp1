function pop = normalMutation(pop, mutateProb, l, args)
% NORMALMUTATION  Apply the normal mutation operation to the population.
%   The argument needed is sigma (the standard deviation of a normal 
%   distribution.

    sigma = args(1);
    
    if (~isnumeric(sigma) || sigma < 0 || sigma > 1)
        sigma = rand;
    end
    
    for i = 1:length(pop)
        if (rand < mutateProb)
            pop(i) = pop(i) + normrnd(0, 1) * sigma;
        end
    end
end