function pop = uniformMutation(pop, mutateProb, l, args)
% UNIFORMMUTATION  Apply the uniform mutation operation to the population.
%   The argument needed are the boundaries.

    lowerBound = args(1);
    upperBound = args(2);

    for i = 1:length(pop)
        if (rand < mutateProb)
            pop(i) = lowerBound + rand * (upperBound - lowerBound);
        end
    end
end