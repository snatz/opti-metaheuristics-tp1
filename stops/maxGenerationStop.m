function stopping = maxGenerationStop(g, history, args)
    Gmax = args(1);
    
    if (g <= Gmax)
        stopping = 0;
    else
        stopping = 1;
    end
end

