function [result, history] = optimize(fitnessFn, selectionFn, crossoverFn, ...
    mutationFn, narrowFn, stopFn, constraints, config, maximizing)

    if (maximizing == 1)
        [result, history] = maximize(fitnessFn, selectionFn, crossoverFn, ...
            mutationFn, narrowFn, stopFn, constraints, config);
    else
        [result, history] = minimize(fitnessFn, selectionFn, crossoverFn, ...
            mutationFn, narrowFn, stopFn, constraints, config);
    end
    
    % Draw graphes
    drawGraphes(fitnessFn, constraints, history, result, maximizing);
end