function r = rosenbrock(x, y)
    sqX = x.^2;
    r = -1.*((1 - sqX) + 100.*(y - sqX).^2);
end