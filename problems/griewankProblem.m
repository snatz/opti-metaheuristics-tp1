function prob = griewankProblem
    prob.constraints = [ -30 30; -30 30 ];
    prob.objectiveFn = @griewank;
end