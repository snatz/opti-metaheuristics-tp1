function prob = rosenbrockProblem
    prob.constraints = [ 0 2; 0 3 ];
    prob.objectiveFn = @rosenbrock;
end