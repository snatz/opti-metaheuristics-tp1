function pop = relativeNarrow(pop, constraints)
% RELATIVENARROW  Apply the relative narrow formula in case the values of 
% the population are outside the boundaries.
%   Warning: does not work in every case. If pop(i) is too small or too big
%   relatively to constraints, pop(i) is still out of the boundaries. The 
%   program might crash or return the wrong values if that happens.

    for i = 1:length(pop)
        if (pop(i) < constraints(1))
            pop(i) = 2*constraints(1) - pop(i);
        elseif (pop(i) > constraints(2))
            pop(i) = 2*constraints(2) - pop(i);
        end
    end
end
