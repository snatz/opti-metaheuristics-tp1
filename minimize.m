function [result, history] = minimize(fitnessFn, selectionFn, ...
    crossoverFn, mutationFn, narrowFn, stopFn, constraints, config)

    % Minimizing f is maximizing -f.
    [result, history] = maximize(@(varargin) -1*fitnessFn(varargin{:}), ...
        selectionFn, crossoverFn, mutationFn, narrowFn, stopFn, ...
        constraints, config);
    
    history(:, 3) = arrayfun(@(x) x * -1, history(:, 3));
    result(3) = result(3) * -1;
end