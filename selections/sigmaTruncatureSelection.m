function selected = sigmaTruncatureSelection(fitnesses, args)
% SIGMATRUNCATURESELECTION  Select the individuals based on the sigma
% truncature fitness scaling and the roulette wheel selection algorithms.
% The argument needed is c (in [1,5], integer).

    N = length(fitnesses);
    av = mean(fitnesses);
    sig = std(fitnesses);

    c = args(1);
    fitnessesScaled = zeros(N, 1);
    
    if (c < 1 || c > 5)
        c = randi(5);
    end
    
    for f = 1:N
        fitnessesScaled(f) = fitnesses(f) - (av - c * sig);
        
        if (fitnessesScaled(f) < 0)
            fitnessesScaled(f) = 0;
        end
    end

    selected = rouletteWheelSelection(fitnessesScaled, args);
end

