function selected = alphaBetaLinearRankingSelection(fitnesses, args)
% ALPHABETALINEARRANKINGSELECTION  Select the individuals based on a ranking 
% algorithm (alpha-beta linear). The arguments needed are alpha and beta
% (alpha + beta must be equals to 2).

    N = length(fitnesses);
    alpha = args(1);
    beta = args(2);
    fitSorted = sort(fitnesses, 'descend');
    sortedIndexes = zeros(N, 1);
    
    if (alpha + beta ~= 2)
        alpha = rand() * 2;
        beta = 2 - alpha;
    end
    
    for i = 1:N
        sortedIndexes(i) = find(fitSorted == fitnesses(i), 1) - 1;
    end
    
    % Fitness-relative probabilities of individuals
    probabilities = zeros(N, 1);
    
    % Probabilities computation
    for prob = 1:N
        probabilities(i) = (alpha + (sortedIndexes(i) * (beta - alpha))/(N-1))/N;
    end
    
    % Selection
    selected = wheelSelection(probabilities, rand(N, 1));
end
