function selected = wheelSelection(probabilities, pointers)
% WHEELSELECTION  Select the individuals based on the wheel selection
% algorithm. The pointers are user-provided.

    % Population length
    N = length(probabilities);
    
    % Selected individuals
    selected = zeros(N, 1);
    
    % Selection
    for p = 1:N
        i = 1;
        
        while pointers(p) > sum(probabilities(1:i))
            i = i + 1;
            
            if (i >= N)
                break
            end
        end

        selected(p) = i;
    end
end

