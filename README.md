## M�taheuristiques en optimisation - TP 1 

Simon Bar & Thomas D�fossez

### Fait

* Algorithme g�n�tique de base
* La possibilit� de traiter les probl�mes de minimisation et les valeurs n�gatives
* Graphes
* Multi-point crossover
* Whole arithmetic crossover
* Local arithmetic crossover
* Uniform crossover
* Blend crossover
* Simulated binary crossover
* Polynomial mutation
* Uniform mutation
* Boundary mutation
* Nonuniform mutation
* Normal mutation
* La s�lection par la m�thode du tournoi
* Stochastic universal sampling
* Le changement d'�chelle lin�aire
* Troncature sigma
* Les m�thodes de classement
* Les deux m�thodes permettant d'assurer la faisabilit� d'une solution
* Op�rateur de croisement issu de la litt�rature (shuffle crossover)
* Les crit�res d'arr�t
* La strat�gie de l'algorithme _steady state_
