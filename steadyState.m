function [history, g] = steadyState(fitnessFn, selectionFn, ...
    crossoverFn, mutationFn, narrowFn, stopFn, constraints, config, ...
    popX, popY)

    lambda = config.lambda;
    
    N = config.N;
    l = config.l;
    Gmax = config.maxGen;
    pm = config.pm;
    pc = config.pc;
    
    xMin = constraints(1, 1);
    xMax = constraints(1, 2);
    yMin = constraints(2, 1);
    yMax = constraints(2, 2);
    
    fitnesses = zeros(N, 1);
    history = zeros(Gmax, 3);
    g = 1;

    for p = 1:N
    	fitnesses(p) = fitnessFn(popX(p), popY(p));
    end
    
    while (~stopFn(g, history, config.stopArgs))

        fitnessesN = normalize(fitnesses);
        selected = selectionFn(fitnessesN, config.selectionArgs);
        matingPerm = randperm(length(selected));
        matingPool = selected(matingPerm);
        matingPool = matingPool(1:2);
        childrenX = crossoverFn(popX(matingPool), pc, l, config.crossArgs);
        childrenY = crossoverFn(popY(matingPool), pc, l, config.crossArgs);
        childrenX = mutationFn(childrenX, pm, l, config.mutationArgs);
        childrenY = mutationFn(childrenY, pm, l, config.mutationArgs);
        childrenX = narrowFn(childrenX, constraints(1, :));
        childrenY = narrowFn(childrenY, constraints(2, :));
        
        for i = 1:lambda
            if min(fitnesses) < fitnessFn(childrenX(i), childrenY(i))
                [Value, Index] = min(fitnesses);
                popX(Index) = childrenX(1);
                popY(Index) = childrenY(1);
                fitnesses(Index) = fitnessFn(childrenX(i), childrenY(i));
            end
        end

        currentBest = pickBest(popX, popY, fitnessFn);
        history(g, :) = currentBest(:);
        
        g = g + 1;
    end
        
end