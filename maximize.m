function [result, history] = maximize(fitnessFn, selectionFn, ...
    crossoverFn, mutationFn, narrowFn, stopFn, constraints, config)

    N = config.N;
    l = config.l;
    Gmax = config.maxGen;
    pm = config.pm;
    pc = config.pc;
    
    xMin = constraints(1, 1);
    xMax = constraints(1, 2);
    yMin = constraints(2, 1);
    yMax = constraints(2, 2);
    
    history = zeros(Gmax, 3);
    g = 1;
    
    % Initialize the populations
    popX = initPopulation(N, xMin, xMax);
    popY = initPopulation(N, yMin, yMax);
    
    % Fitness results
    fitnesses = zeros(N, 1);
    
    % Check if the steady state algorithm must be used.
    if config.lambda == 1 || config.lambda == 2
        [history, g] = steadyState(fitnessFn, selectionFn, crossoverFn, ...
            mutationFn, narrowFn, stopFn, constraints, config, popX, popY);
    else
        while (~stopFn(g, history, config.stopArgs))        
            % Fitness results
            for p = 1:N
                fitnesses(p) = fitnessFn(popX(p), popY(p));
            end

            % Normalization of negative fitnesses values.
            fitnesses = normalize(fitnesses);

            % Mating pool selection
            selected = selectionFn(fitnesses, config.selectionArgs);

            % Assignment of parents pairs by permutation (1-2, 3-4, ...)
            matingPerm = randperm(length(selected));
            matingPool = selected(matingPerm);

            % Crossover
            childrenX = crossoverFn(popX(matingPool), pc, l, config.crossArgs);
            childrenY = crossoverFn(popY(matingPool), pc, l, config.crossArgs);

            % Mutation
            childrenX = mutationFn(childrenX, pm, l, config.mutationArgs);
            childrenY = mutationFn(childrenY, pm, l, config.mutationArgs);

            % Check if the solution is feasible.
            popX = narrowFn(childrenX, constraints(1, :));
            popY = narrowFn(childrenY, constraints(2, :));

            % Record the best individual of the generation.
            currentBest = pickBest(popX, popY, fitnessFn);
            history(g, :) = currentBest(:);

            g = g + 1;
        end
    end
    
    % Pick the best individual across every generation.
    result = pickBest(history(:, 1), history(:, 2), fitnessFn);
    
    % Shrink history if necessary (for graphes).
    history = history(1:g-1, :);
end