function config = geneticConfig(N, maxGen, chromLen, crossProb, mutProb, ...
    lambda, selectionArgs, crossoverArgs, mutationArgs, stopArgs)
    % Configuration containing the population size, max number of 
    % generation, probabilities of crossover and mutation, chromosome 
    % length, and additional arguments.
    config.N = N;
    config.maxGen = maxGen;
    config.l = chromLen;
    config.pc = crossProb;
    config.pm = mutProb;
    config.selectionArgs = selectionArgs;
    config.crossArgs = crossoverArgs;
    config.mutationArgs = mutationArgs;
    config.stopArgs = stopArgs;
    config.lambda = lambda;
end