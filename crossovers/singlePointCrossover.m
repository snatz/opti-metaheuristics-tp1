function c = singlePointCrossover(matingPool, pc, L, args)
% SINGLEPOINTCROSSOVER Apply the single point crossover operation to the 
%   mating pool. The arguments needed are the constraints.

    c = zeros(length(matingPool), 1);
    constraints = args(1);
    
    for i = 1:length(matingPool)/2
        % No crossover here.
        if (rand > pc)
            c(2*i - 1) = matingPool(2*i - 1);
            c(2*i) = matingPool(2*i);
            continue;
        end
        
        crossPt = randi(L - 1);
        
        b1 = float2bin(matingPool(2*i - 1), L, constraints(1));
        b2 = float2bin(matingPool(2*i), L, constraints(1));

        cb1 = strcat(b1(1:crossPt), b2(crossPt+1:end));
        cb2 = strcat(b2(1:crossPt), b1(crossPt+1:end));
        
        % Children.
        c(2*i - 1) = bin2float(cb1, constraints(1));
        c(2*i) = bin2float(cb2, constraints(1));
    end
end