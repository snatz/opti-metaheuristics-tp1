function c = wholeArithmeticCrossover(matingPool, pc, L, args)
% WHOLEARITHMETICCROSSOVER  Apply the whole arithmetic crossover operation 
%   to the mating pool.

    c = zeros(length(matingPool), 1);
    alpha = rand;

    for i = 1:length(matingPool)/2
        % No crossover here.
        if (rand > pc)
            c(2*i - 1) = matingPool(2*i - 1);
            c(2*i) = matingPool(2*i);
            continue;
        end
        
        beta = 1 - alpha;

        parent1 = matingPool(2*i - 1);
        parent2 = matingPool(2*i);
        
        % Children.
        c(2*i - 1) = alpha*parent1 + (1 - alpha)*parent2 ;
        c(2*i) = beta*parent1 + (1 - beta)*parent2;
    end
end

