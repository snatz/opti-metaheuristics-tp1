function c = localArithmeticCrossover(matingPool, pc, L, args)
% LOCALARITHMETICCROSSOVER  Apply the local arithmetic crossover operation 
%   to the mating pool.

    c = zeros(length(matingPool), 1);
    
    % One alpha different per individual.
    alpha = rand(length(matingPool)/2, 1);

    for i = 1:length(matingPool)/2
        % No crossover here.
        if (rand > pc)
            c(2*i - 1) = matingPool(2*i - 1);
            c(2*i) = matingPool(2*i);
            continue;
        end
        
        beta = 1 - alpha(i);

        parent1 = matingPool(2*i - 1);
        parent2 = matingPool(2*i);
        
        % Children.
        c(2*i - 1) = alpha(i)*parent1 + (1 - alpha(i))*parent2 ;
        c(2*i) = beta*parent1 + (1 - beta)*parent2;
    end
end

