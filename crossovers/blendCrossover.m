function c = blendCrossover(matingPool, pc, L, args)
% BLENDCROSSOVER  Apply the blend crossover operation to the mating pool.
%   The arguments needed are alpha (in [0,1]), and the lower/upper
%   boundaries.

    c = zeros(length(matingPool), 1);
    alpha = args(1);
    constraints(1) = args(2);
    constraints(2) = args(3);
    
    if (alpha < 0 || alpha > 1)
        alpha = rand;
    end
    
    for i = 1:length(matingPool)/2
        % No crossover here.
        if (rand > pc)
            c(2*i - 1) = matingPool(2*i - 1);
            c(2*i) = matingPool(2*i);
            continue;
        end
        
        parent1 = matingPool(2*i - 1);
        parent2 = matingPool(2*i);
        
        if (parent1 < parent2)
            minR = parent1 - alpha*(parent2 - parent1);
            maxR = parent2 + alpha*(parent2 - parent1);
        else
            minR = parent2 - alpha*(parent1 - parent2);
            maxR = parent1 + alpha*(parent1 - parent2);
        end
        
        % Children.
        c(2*i - 1) = minR + rand * (maxR - minR);
        c(2*i) = minR + rand * (maxR - minR);
    end
    
    % Values can be evolve to be outside of the boundaries.
    c = strictNarrow(c, constraints);
end