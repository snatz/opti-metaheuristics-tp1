function c = multiPointCrossover(matingPool, pc, L, args)
% MULTIPOINTCROSSOVER  Apply the multi point crossover operation to the 
%   mating pool. The arguments needed are the boundaries and the number of
%   points.

    c = zeros(length(matingPool), 1);
    constraints = args(1);
    nPoints = args(2);
    crossPts = zeros(nPoints, 1);
    
    if (nPoints < 1 || nPoints > L - 1)
        c = matingPool;
        return;
    end
    
    for i = 1:length(matingPool)/2
        % No crossover here.
        if (rand > pc)
            c(2*i - 1) = matingPool(2*i - 1);
            c(2*i) = matingPool(2*i);
            continue;
        end
        
        for p = 1:nPoints
            crossPts(i) = randi(L - 1);
        end
        
        b1 = float2bin(matingPool(2*i - 1), L, constraints(1));
        b2 = float2bin(matingPool(2*i), L, constraints(1));
        cb1 = b1;
        cb2 = b2;

        for p = 1:nPoints
            if (p == 1)
                cb1(1:crossPts(p)) = b2(1:crossPts(p));
                cb2(1:crossPts(p)) = b1(1:crossPts(p));
            elseif (p == nPoints && mod(p, 2) == 0)
                cb1(crossPts(p)+1:end) = b1(crossPts(p)+1:end);
                cb2(crossPts(p)+1:end) = b2(crossPts(p)+1:end);
            elseif (p == nPoints && mod(p, 2) == 1)
                cb1(crossPts(p)+1:end) = b2(crossPts(p)+1:end);
                cb2(crossPts(p)+1:end) = b1(crossPts(p)+1:end);
            elseif (mod(p, 2) == 0)
                cb1(crossPts(p)+1:crossPts(p+1)) = b1(crossPts(p)+1:crossPts(p+1));
                cb2(crossPts(p)+1:crossPts(p+1)) = b2(crossPts(p)+1:crossPts(p+1));
            elseif (mod(p, 2) == 1)
                cb1(crossPts(p)+1:crossPts(p+1)) = b2(crossPts(p)+1:crossPts(p+1));
                cb2(crossPts(p)+1:crossPts(p+1)) = b1(crossPts(p)+1:crossPts(p+1));
            end
        end
        
        child1 = bin2float(cb1, constraints(1));
        child2 = bin2float(cb2, constraints(1));
        
        c(2*i - 1) = child1;
        c(2*i) = child2;
    end
end