function c = uniformCrossover(matingPool, pc, L, args)
% UNIFORMCROSSOVER  Apply the uniform crossover operation to the mating 
%   pool. The arguments needed are the constraints and p (in [0,1]).

    c = zeros(length(matingPool), 1);
    constraints = args(1);
    p = args(2);
    
    if (p < 0 || p > 1)
        p = 0.5;  % default value
    end
     
    for i = 1:length(matingPool)/2
        % No crossover here.
        if (rand > pc)
            c(2*i - 1) = matingPool(2*i - 1);
            c(2*i) = matingPool(2*i);
            continue;
        end
        
        b1 = float2bin(matingPool(2*i - 1), L, constraints(1));
        b2 = float2bin(matingPool(2*i), L, constraints(1));

        cb1 = b1;
        cb2 = b2;
        
        for b = 1:L
            if (rand < p)
                cb1(b) = b2(b);
            end
            
            if (rand < p)
                cb2(b) = b1(b);
            end
        end
        
        % Children.
        c(2*i - 1) = bin2float(cb1, constraints(1));
        c(2*i) = bin2float(cb2, constraints(1));
    end
end