function c = shuffleCrossover(matingPool, pc, L, args)
% SHUFFLECROSSOVER Apply the suffle crossover operation to the 
%   mating pool. The arguments needed are the constraints.
%   Ref:
%   http://www.zemris.fer.hr/~golub/clanci/B/WSEAS_TRANSACTIONSonCOMPUTERS_88-211.pdf

    c = zeros(length(matingPool), 1);
    constraints = args(1);
    
    for i = 1:length(matingPool)/2
        % No crossover here.
        if (rand > pc)
            c(2*i - 1) = matingPool(2*i - 1);
            c(2*i) = matingPool(2*i);
            continue;
        end
        
        crossPt = randi(L - 1);
        
        b1 = float2bin(matingPool(2*i - 1), L, constraints(1));
        b2 = float2bin(matingPool(2*i), L, constraints(1));
        
        %Randomly shuffle parents bits
        shuffle = randperm(L);
        b1 = b1(shuffle);
        b2 = b2(shuffle);

        cb1 = strcat(b1(1:crossPt), b2(crossPt+1:end));
        cb2 = strcat(b2(1:crossPt), b1(crossPt+1:end));
        
        %Reverse shuffle
        cb1(shuffle) = cb1;
        cb2(shuffle) = cb2;

        % Children.
        c(2*i - 1) = bin2float(cb1, constraints(1));
        c(2*i) = bin2float(cb2, constraints(1));
    end
end